import { useEffect, useReducer, useState } from "react";
import {
  Card,
  Select,
  MenuItem,
  Button,
  Box,
  Modal,
  CircularProgress,
} from "@mui/material";
import { Add, Remove } from "@mui/icons-material/";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";

import CityApi from "../services/cityApis";

const style = {
  position: "absolute",
  top: "30%",
  left: "54.5%",
  width: 140,
  bgcolor: "background.paper",
  border: "1px solid #000",
  boxShadow: 24,
};

const initialState = {
  open: false,
  error: "",
  loading: false,
  city: [],
  traveller: 0,
};

const ACTION = {
  START: "start loading",
  OPEN_MODAL: "open modal",
  CLOSE_MODAL: "close modal",
  INCREMENT_TRAVELLERS: "increase travellers",
  DECREMENT_TRAVELLERS: "decrease travellers",
  SET_CITY: "set city data",
  ERROR: "set error",
};

function reducer(state, action) {
  switch (action.type) {
    case ACTION.START: {
      return { ...state, loading: true };
    }
    case ACTION.SET_CITY: {
      return {
        ...state,
        city: [...state.city, action.payload.data],
        loading: false,
      };
    }
    case ACTION.ERROR: {
      return { ...state, error: action.payload.data, loading: false };
    }
    case ACTION.INCREMENT_TRAVELLERS: {
      return { ...state, traveller: state.traveller + 1 };
    }
    case ACTION.DECREMENT_TRAVELLERS: {
      return { ...state, traveller: state.traveller - 1 };
    }
    case ACTION.OPEN_MODAL: {
      return { ...state, open: true };
    }
    case ACTION.CLOSE_MODAL: {
      return { ...state, open: false };
    }
    default:
      return state;
  }
}

const SearchBar = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [fromCity, setFromCity] = useState("");
  const [toCity, setToCity] = useState("");

  const { city, open, traveller, error, loading } = state;

  useEffect(() => {
    dispatch({ type: ACTION.START });
    CityApi.getAllCities()
      .then((res) => {
        dispatch({ type: ACTION.SET_CITY, payload: { data: res.data.data } });
      })
      .catch((err) =>
        dispatch({ type: ACTION.ERROR, payload: { data: err.message } })
      );
  }, []);

  return (
    <div style={{ marginTop: "50px" }}>
      {loading && <CircularProgress />}
      {error && <p style={{ color: "red" }}>{error}</p>}
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          height: "auto",
        }}
      >
        <Card sx={{ width: "80%", padding: "16px" }}>
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              gap: "8px",
            }}
          >
            <Select
              displayEmpty
              value={fromCity}
              onChange={(event) => setFromCity(event.target.value)}
              style={{ minWidth: "400px" }}
            >
              <MenuItem value="">From</MenuItem>
              {city.map((data) => (
                <MenuItem key={data.id} value={data.name}>
                  {data.name}
                </MenuItem>
              ))}
            </Select>
            <Select
              displayEmpty
              value={toCity}
              onChange={(event) => setToCity(event.target.value)}
              style={{ minWidth: "400px" }}
            >
              <MenuItem value="">To</MenuItem>
              {city.map((data) => (
                <MenuItem key={data.id} value={data.name}>
                  {data.name}
                </MenuItem>
              ))}
            </Select>
            <Button
              style={{
                color: "grey",
                border: "0.5px solid grey",
                height: "55px",
              }}
              onClick={() => dispatch({ type: ACTION.OPEN_MODAL })}
            >
              Passengers
            </Button>
            <Modal
              open={open}
              onClose={() => dispatch({ type: ACTION.CLOSE_MODAL })}
            >
              <Box sx={style}>
                <Button
                  onClick={() =>
                    dispatch({ type: ACTION.INCREMENT_TRAVELLERS })
                  }
                >
                  {" "}
                  <Add></Add>
                </Button>
                {traveller}
                <Button
                  onClick={() =>
                    traveller > 0 &&
                    dispatch({ type: ACTION.DECREMENT_TRAVELLERS })
                  }
                >
                  {" "}
                  <Remove></Remove>{" "}
                </Button>
              </Box>
            </Modal>
            <Select displayEmpty value="" style={{ minWidth: "150px" }}>
              <MenuItem value="">Sort</MenuItem>
              <MenuItem value="Price">Cheapest</MenuItem>
              <MenuItem value="Arrival Time">Early Arrival</MenuItem>
              <MenuItem value="Departure Time">Early Departure</MenuItem>
            </Select>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DatePicker onAccept={(event) => console.log(event.$d)} />
            </LocalizationProvider>
            <Button
              variant="contained"
              color="primary"
              onClick={() => console.log("Clicked")}
            >
              Search
            </Button>
          </Box>
        </Card>
      </Box>
    </div>
  );
};

export default SearchBar;
