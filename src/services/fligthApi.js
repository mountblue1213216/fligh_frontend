import axios from "axios";

class Flight {
  getFlights() {
    return axios.get(`http://localhost:3000/api/v1/flights/`);
  }
}

export default new Flight();
