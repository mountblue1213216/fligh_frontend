import axios from "axios";

class City {
  getAllCities() {
    return axios.get("http://localhost:3000/api/v1/cities");
  }
}

export default new City();
